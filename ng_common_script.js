// Main Nav

window.addEventListener('resize', function(event) {
    // Keeps proper nav width even when resizing window.
    document.getElementById("main-nav").classList.remove("grow-width");
}, true);

function openNav() {
    document.getElementById("main-nav").classList.add("grow-width");
}
  
function closeNav() {
    document.getElementById("main-nav").classList.remove("grow-width");
}
