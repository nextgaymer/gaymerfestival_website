
// Carousels

function* infiniteGenerator (iterable) {
    var index = 0;
    while (true) {
        if (index < iterable.length) {
            yield iterable[index];
        }
        else {
            index = 0;
            yield iterable[0];
        }
        index++;
    }
}

function changeImage(imageId, imageGenerator){
    var img = document.getElementById(imageId);
    img.classList.remove("show");
    img.classList.add("fade");
    setTimeout(function(){
        imgData = imageGenerator.next().value;
        img.src = imgData.src;
        img.alt = imgData.alt;
    }, 500);
    setTimeout(function(){
        img.classList.remove("fade");
        img.classList.add("show");
    }, 500);
}

let comedyImages = [
    {
        src: "new_images/gf2024/artistes/louis.jpg",
        alt: "Une photo de l'humoriste Louis Catellat."
    },
    {
        src: "new_images/gf2024/artistes/comedy-pride.jpeg",
        alt: "Le logo du collectif Comedy Pride."
    },
    {
        src: "new_images/gf2024/artistes/tahnee.jpeg",
        alt: "Une photo de l'humoriste Tahnee."
    }
];
// Pre-load images in cache to avoid display flicker the first time
for (const imgData of comedyImages) {new Image().src=imgData.src;};
let comedyBubbleId = "comedy-bubble";
let comedyGenerator = infiniteGenerator(comedyImages);
setInterval(function() {changeImage(comedyBubbleId, comedyGenerator);}, 4000);

let dragImages = [
    {
        src: "new_images/gf2024/artistes/dnd.jpg",
        alt: "Le logo du spectacle Donjons & Dragout du collectif Drag La Cousinade."
    },
    {
        src: "new_images/gf2024/artistes/soa.jpg",
        alt: "Une photo de l'artiste Drag Soa de Muse."
    }
];
// Pre-load images in cache to avoid display flicker the first time
for (const imgData of dragImages) {new Image().src=imgData.src;};
let dragBubbleId = "drag-bubble";
let dragGenerator = infiniteGenerator(dragImages);
setInterval(function() {changeImage(dragBubbleId, dragGenerator);}, 5000);

function openComedyArtists () {
    element = document.getElementById("comedy-artists");
    if (element.style.display == "none") {
        element.style.display = "block";
    }
    else {
        element.style.display = "none";
    }
}

const randomProperties = function (particle) {
    const left = Math.random() * 100;
    particle.style.setProperty('--left', left + '%');
  
    const body = document.body;
    const html = document.documentElement;
    const documentHeight = Math.max(body.scrollHeight, body.offsetHeight, 
        html.clientHeight, html.scrollHeight, html.offsetHeight );

    const top = Math.random() * documentHeight - 50.;
    particle.style.setProperty('--top', top + 'px');
  
    const size = Math.floor(Math.random() * (6 - 2)) + 2;
    particle.style.setProperty('--size', size + 'px');
    particle.style.setProperty('--blur', (size * 4) + 'px');
    particle.style.setProperty('--spread', (size) + 'px');
  
    const opacity = Math.random() + 0.1;
    particle.style.setProperty('--opacity', opacity);
  
    const duration = Math.floor(Math.random() * (3000 - 1500)) + 1500;
    particle.style.setProperty('--duration', duration + 'ms');
  
    const delay = Math.floor(Math.random() * (3000 - 1500)) + 1500;
    particle.style.setProperty('--delay', delay + 'ms');
  
    const iteration = Math.floor(Math.random() * (100000000 - 4)) + 4;
    particle.style.setProperty('--iteration', iteration);
  };
  
  const removeSparkles = function() {
    let sparkles = document.getElementsByClassName('particle');
  
    for (const sparkle of sparkles) {
      sparkle.parentNode.removeChild(sparkle);
    }
  };
  
  const addSparkles = function() {
    let maxCount = 200;
  
    for (let i = 0; i < maxCount; i++) {
      let sparkle = document.createElement("div");
      sparkle.classList.add("particle");
  
      let main = document.querySelector('main');
      main.appendChild(sparkle);
  
      randomProperties(sparkle);
    }
  };

  addSparkles();
